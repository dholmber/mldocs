# Notebooks

To provide full data science experience, ml.cern.ch offers Jupyter Notebooks.  
Every Notebook server can be customized, by installing required packages.  
For every Notebook server, resources such as CPUs or GPUs can be added.  

<a name="create"></a>To create a Notebook server:

1. Go to [https://ml.cern.ch/_/jupyter/](https://ml.cern.ch/_/jupyter/) .  
2. Select **New Server** .  
3. Select a name that starts with a letter (not a number).  
4. Select an image.  
    * From the list of [base images](#base-images).  
    * Use a [custom image](#custom-images).  
5. Select number of CPUs as needed.  
6. Select number of GPUs as needed.  
7. Wait until notebook server starts, then click **Connect** .  

## Base Images

The fastest way to start development is to select one of pre-built images to run a Jupyter notebook.  

Pre-built images contain most functionalities to provide machine learning training and integrate with existing CERN services.  

Pre-built images contain:  

* Python 3.  
* TensorFlow or TensorFlow-gpu.  
* Integration with EOS.  
* Integration with Kubeflow components (pipelines, Katib, KALE).  
    
If additional functionalities or libraries are required:  

* They can be installed in the running server with: `pip3 install LIBRARY --user` .  
* Or [custom image](#custom-images) can be built.  

## Custom Images

Custom images offer a possibility to create a customized machine learning environment.  
This can be useful for Notebooks, but is especially useful for Pipelines and Katib, where it's not possible to install with pip.

Custom images can be built with the following steps:

* Clone [git repository](https://gitlab.cern.ch/ai-ml/custom_ml_images/) with a basic Dockerfile:  
```
git clone https://gitlab.cern.ch/ai-ml/custom_ml_images.git
```

* Position to the directory:  
```
cd custom_ml_images
```

* Edit _requirements.txt_ to add or remove desired packages.  
* Edit _Dockerfile_, in case other actions are needed.  
* Build Docker image:  
```
docker build . -f Dockerfile -t gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH
```

* Push built custom image to a container registry:  
```
docker push gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH
```
    
* Create the new Notebook Server by following steps [here](#create)
