# [ml.cern.ch](https://ml.cern.ch)

This guide provides details and guidelines for the ml.cern.ch service.

## Description

[**ml.cern.ch**](https://ml.cern.ch) is a centralized service at CERN to run machine learning workloads.  

It allows for creating notebooks, selecting GPU resources, developing ML pipelines, performing hyper parameter optimization, distributed training, among others features.  

The service is built on [**Kubeflow**](https://www.kubeflow.org/), a machine learning platform on Kubernetes.  

This guide dives into the main features of the service, providing a quick onboarding. 

Also [check here](/project/) for additional information about the project, planning,
presentations, etc.
