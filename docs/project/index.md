# ML Project

The project aims at offering a centralized service to manage the full machine
learning lifecycle:

* Data Extraction and Preparation
* Interactive analysis and iteration
* Distributed Training, Hyper Parameter Optimization
* Model Storage, Versioning and Serving

It also offers access to a large amount of accelerator resources like GPUs,
TPUs, IPUs and FPGAs - both on premises and external.

## Meetings

We run bi-weekly meetings for sprints and discussion.

* Every Second Monday 11am - [events here](https://indico.cern.ch/category/14173/)
* [Kanban Board](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=7376)

## Communication

* [Mattermost](https://mattermost.web.cern.ch/it-dep/channels/ml)

## Milestones

### Major


### Ongoing Work

* [Upgrade to Kubeflow 1.3](https://its.cern.ch/jira/browse/OS-12656)
* [Automation of Credential Renewal](https://its.cern.ch/jira/browse/OS-13612)

## Presentations

### Internal

* Machine Learning Coffees, Oct 16 2020: [Making ML easier with Kubeflow](https://indico.cern.ch/event/966345/)
* DUNE, Oct 02 2020: [Kubeflow Overview and Demo](https://indico.cern.ch/event/961276/)

### External

* Kubecon Europe 2021, May 4-7: [Building and Managing a Centralized ML Platform with Kubeflow at CERN](https://kccnceu2021.sched.com/event/iE1w/building-and-managing-a-centralized-ml-platform-with-kubeflow-at-cern-ricardo-rocha-dejan-golubovic-cern?iframe=no&w=100%&sidebar=yes&bg=no)
* Fast Machine Learning for Science Workshop, Dec 01 2020: [Making ML Easier with Kubeflow](https://indico.cern.ch/event/924283/contributions/4105328/)
* 25th International Conference on Computing in High-Energy and Nuclear Physics, May 17-21 2020: [Training and Serving ML workloads with Kubeflow at CERN](https://indico.cern.ch/event/948465/contributions/4323970/)

## References

* [Upstream Documentation](https://www.kubeflow.org/)
