# Model Training

Basic model training can be done via Notebooks. 
To perform basic training:  

* [Create a new notebook server](https://ml.docs.cern.ch/notebooks/).  
* Import preferred framework (TensorFlow, PyTorch, etc).  
* Write code.  

Kubeflow provides additional functionalites for training, that allow for distributing workloads.

## TensorFlow

To run training jobs with TensoFlow, [TFJob](https://www.kubeflow.org/docs/components/training/tftraining/) can be used.  
A TFJob is a custom kubernetes resource that offers a way to integrate with the full set of TensorFlow capabilities.  

It simplifies **distributed training** by providing a scheduling mechanism and automatically setting the configuration of the TF workers.  

A distributed TensorFlow job typically contains 0 or more of the following processes ([source](https://www.kubeflow.org/docs/components/training/tftraining/)):

* Chief - The chief is responsible for orchestrating training and performing tasks like checkpointing the model.  
* PS - The ps are parameter servers; these servers provide a distributed data store for the model parameters.  
* Worker - The workers do the actual work of training the model. In some cases, worker 0 might also act as the chief.  

To run a TFJob:

* Write a Python script with TensorFlow distributed code.  
* Build a Docker image to contain all the dependencies to run the script.  
* Write a .yaml file to specify TensorFlow distribution configuration.  
* Submit .yaml file from a command line or Katib UI.  

### Example  

* Clone [git repository](https://gitlab.cern.ch/ai-ml/examples) . 
    * `git clone https://gitlab.cern.ch/ai-ml/examples.git` .  
* Navigate to: `cd tfjob` .  
* Build docker image with custom code:  
    * `docker build -f Dockerfile -t @username/custom-tfjob .` .  
* Push docker image: `docker push @username/custom-tfjob` .  
* Edit custom-code.yaml.  
    * Select number of TF workers, chief workers and paramter servers.  
    * For each process specify resources (GPUs, TPUs), image, as specified [here](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/tfjob/custom-code.yaml).  
    * Make sure to run from the image you have just built `image: registry.hub.docker.com/@username/custom-tfjob` .  
* Navigate to [Katib Dashboard](https://ml.cern.ch/katib/#/katib/hp), copy/paste content of custom-code.yaml.  
* Click _Deploy_ .  

## PyTorch

PyTorch training is supported in Kubeflow via [PyTorch operator](https://www.kubeflow.org/docs/components/training/pytorch/).  
Using the PyTorch operator, training jobs can be distributed across multiple CPUs or GPUs.

### Example  

* Clone [git repository](https://gitlab.cern.ch/ai-ml/examples) . 
    * `git clone https://gitlab.cern.ch/ai-ml/examples.git` .  
* Navigate to: `cd pytorchjob` .  
* Build docker image with custom code:  
    * `docker build -f Dockerfile -t @username/pytorch-job .` .  
* Push docker image: `docker push @username/pytorch-job` .  
* Edit pytorch.yaml (or other provided .yaml files).  
    * Define PyTorch master and workers. 
    * Make sure to run from the image you have just built `image: registry.hub.docker.com/@username/pytorch-job` .  
* Navigate to [Katib Dashboard](https://ml.cern.ch/katib/#/katib/hp), copy/paste content of custom-code.yaml.  
* Click _Deploy_ .  


