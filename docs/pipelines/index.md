# Pipelines

Machine Learning Pipelines define work-flows in the form of a directed graph.  
Pipelines provide more flexibility to define dependencies between components compared to classic scripts or notebooks.  
For example, with pipelines, multiple models can be trained in parallel or data can be read from multiple streams in parallel.

<video width="427" height="240" controls>
  <source src="https://indico.cern.ch/event/924283/contributions/4105328/attachments/2153724/3632746/Kubeflow%20Demo%20Pipelines.mp4" type="video/mp4">
</video>

## Recurrent

Sometimes, it's needed to have a recurring machine learning job. For example model training with new data every day, or periodic monitoring of services availability, or monitoring usage of a specific system. To do so, a recurrent pipeline can be submitted, specifying various pipeline triggering option, such period between two pipeline runs, start and end date/time or cron options.

## Pipeline Parameters
Every pipeline can have input values that can change every time the pipeline is triggered. 
For example, input data paths, or different options for hyperparameters can be supplied as pipeline parameters, rather than hard coded.  
By default, pipelines are run without any parameters.

## Running a Pipeline

### MNIST - Notebook
* Follow [the guide to setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Open **examples/mnist-kale/mnist-kale-katib.ipynb** in a Notebook server
* On the left side of the Notebook panel, select Kubeflow Pipelines Deployment Panel
* Toggle **Enable**
* Select Experiment (existing or new)
* Write Pipeline name and Pipeline description
* Untoggle **HP Tuning with Katib**
* Click **Compile and Run** at the bottom of the page
* After successfull compilation, click View
* Inspect and debug your pipeline via Pipeline log

### Hello World - Dashboard

To run pipelines from pipeline dashboard, a .yaml file with pipeline definition is needed. It can be a custom pipeline, or one of the provided examples.

* Navigate to [https://ml.cern.ch/_/pipeline/?ns=MY_NAMESPACE](https://ml.cern.ch/_/pipeline/?ns=MY_NAMESPACE)
* Click Upload Pipeline
* Download [the example yaml file](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/argo-workflows/helo-world/dag_diamond.yaml)
* Add Pipeline name
* Add Pipeline description
* **IMPORTANT: Add following to Pipeline Description:**
    * `namespace: {NAMESPACE}`, {NAMESPACE} - personal kubeflow namespace shown in top left corner
* Upload the downloded [example](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/argo-workflows/helo-world/dag_diamond.yaml)
* Click Create
* Click **Create Run**
    * Select Run name
    * Select Experiment in which to run the pipeline. An experiment holds multiple runs of the same pipeline, and make it easier to track the runs. If needed, create an Experiment first.
    * Select if the pipeline is run once, or is [recurrent](#recurrent)
      * If recurrent, select pipeline trigger options (period, start and end date/time)
    * Select [Pipeline Parameters](#pipeline-parameters)
    * Click **Start**
* Click on the name of the running pipeline (Run name)
* Track the progress of the pipeline

### EOS With S3 Secrets - Terminal

* Connect to a running notebook server - [ml.cern.ch/_/jupyter/](https://ml.cern.ch/_/jupyter/)
    * If needed, follow [the guide to setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* Open a new Terminal instance
* In Terminal, clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/argo-workflows/eos_and_secrets**
```
cd examples/argo-workflows/eos_and_secrets
```
* Login to kerberos with kinit:
```
kinit CERN-USER-ID
```
* Create a kerberos secret for Kubernetes.  
```
kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000
```
* Edit `s3_secret.yaml` to add credentials for accessing a bucket.  
* Create a secret for S3 bucket.  
```
kubectl apply -f s3_secret.yaml
```
* Customize `argo_eos_secrets.yaml` file.  
    * Change user directory in EOS.  
    * Use custom image if needed.  
    * Use custom command and scripts.  
* Upload the pipeline. Note: {NAMESPACE} is a personal Kubeflow namespace.  
```
kfp pipeline upload -p eos_secrets_pipeline -d 'namespace: {NAMESPACE}' argo_eos_secrets.yaml
```
* Create an experiment (a wrapper for pipeline runs).  
```
kfp experiment create argo_experiment
```
* Run the pipeline.  
```
kfp run submit -e argo_experiment -n eos_secrets_pipeline -r first_run -w
```
* Monitor pipeline completion from Terminal.  
    * Or use UI to explore pipeline logs - [ml.cern.ch/_/pipeline/#/experiments](https://ml.cern.ch/_/pipeline/#/experiments).  

### TensorBoard - Notebook

[TensorBoard](https://www.tensorflow.org/tensorboard) provides the visualization and tooling needed for machine learning experimentation:

* Tracking and visualizing metrics such as loss and accuracy.  
* Visualizing the model graph (ops and layers).  
* Viewing histograms of weights, biases, or other tensors as they change over time.  
* Projecting embeddings to a lower dimensional space.  
* Displaying images, text, and audio data.  
* Profiling TensorFlow programs.  

This example covers a pipeline whose **first component** is **TensorBoard**, to monitor the training process in the subsequent steps.  

The **second component** is **training**. A model is being trained and stored in a **GCP bucket**.

The **third component** is **metrics evaluation**. 

The **fourth component** provides **model serving**. Users can query the trained model from the previous step.

To run:

* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/tensorboard/** in a Notebook server.  
* Open [keras-tuner-nb.ipynb](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/tensorboard/keras-tuner-nb.ipynb) notebook.  
* Select WORKING_DIR to be an accessible GCP bucket.  
* Run the cells.  
* The result should be a successfully completed pipeline.   
![bikesw-pipeline](images/bikesw-pipeline.png)

### Distributed Training - Notebook

Purpose of this pipeline is to encapsulate a distributed training as one of the pipeline components.  
Distributed training is provided with Kubeflow [TFJob](https://www.kubeflow.org/docs/components/training/tftraining/).  

**First component** is **TensorBoard**, to monitor the training process in the subsequent steps.  

**Second component** is **training**. A model is being trained accross **multiple GPUs** and stored in a **GCP bucket**.

**Third component** provides **model serving**. Users can query the trained model from the previous step.

To run:

* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/tfjob_tensorboard/** in the Notebook server.  
* Open [keras-tfjob-pipeline.ipynb](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/tfjob_tensorboard/keras-tfjob-pipeline.ipynb) notebook.  
* Select WORKING_DIR to be an accessible GCP bucket.  
* Select number of train epochs.  
* Run the cells.  
* Wait until the pipeline completes, result should be a successfully completed pipeline.   
![bikesw-pipeline](images/bikesw-pipeline-tfjob.png)
* Obtain a session Cookie value from the browser (In Chrome: View -> Developer -> Developer Tools -> Network)
    * Copy the value to the **cookie** file.  
* Query the serving model in the last cell.  
* Expected output  
```
{
    "predictions": [[1042.16382]
    ]
}
```
