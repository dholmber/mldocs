[Kubeflow Fairing](https://www.kubeflow.org/docs/components/fairing/) is a Python package that makes it easy to train and deploy ML models on [Kubeflow](https://www.kubeflow.org/docs/about/kubeflow/).  
  
Kubeflow Fairing can also been extended to train or deploy on other platforms. Currently, Kubeflow Fairing has been extended to train on Google AI Platform.  
  
Kubeflow Fairing packages a Jupyter notebook, Python function, or Python file as a Docker image, then deploys and runs the training job on Kubeflow or AI Platform. After the training job is complete, Kubeflow Fairing can be used to deploy the trained model as a prediction endpoint on Kubeflow.  

## Example

* Connect to a running Notebook server
    * If needed, follow [the guide for setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/)
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Navigate to **examples/fairing/** in a Notebook server

* Obtain an Authentication Session Cookie from Chrome
    * Click `View -> Developer -> Developer Tools -> Network`
    * Navigate to [ml.cern.ch](https://ml.cern.ch)
    * Check Request Headers
        * Copy section `authservice_session` to the [cookie](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/fairing/cookie) file

* Have a Docker registry username and password handy

* Open notebook at [fairing-mnist.ipynb](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/fairing/fairing-mnist.ipynb)

* Fill in Docker credentials

* Run the cells
    * Perform distributed training
    * Serve the trained model
    * Run the inference service using the input file
