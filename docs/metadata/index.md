# Kubeflow Metadata

The goal of the [Kubeflow Metadata](https://www.kubeflow.org/docs/components/metadata/) project is to help users understand and manage their machine learning workflows by tracking and managing the metadata that the workflows produce.  
  
In this context, metadata means information about executions (runs), models, datasets, and other artifacts.  
_Artifacts_ are the files and objects that form the inputs and outputs of the components in a ML workflow.  
Information stored about an artifact can include: name, version, storage url or custom labels.  
Accessing Metadata Artifacts Store can be done using Python SDK or [Kubeflow UI](https://ml-staging.cern.ch/_/metadata/).  

## Example

Example of how to use the Metadata SDK can be found in this [demo notebook](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/metadata/demo_metadata.ipynb).

To run the notebook in your Kubeflow cluster:

* Follow [the guide to setting up Jupyter notebooks in Kubeflow](https://ml.docs.cern.ch/notebooks/).
* In the Notebook terminal clone [git repository](https://gitlab.cern.ch/ai-ml/examples) with examples
```
git clone https://gitlab.cern.ch/ai-ml/examples.git
```
* Open notebook at **examples/metadata/demo_metadata.ipynb**
* Run the steps in the notebook to install and use the Metadata SDK

After running the steps in the notebook, result metadata can be viewed in Kubeflow UI:  

* Click **Artifact Store** in the left-hand navigation panel on the Kubeflow UI.  
* On the **Artifacts** screen you should see the following items:
    * A **model** metadata item with the name MNIST
    * A **metrics** metadata item with the name MNIST-evaluation
    * A **dataset** metadata item with the name mytable-dump

## Metadata SDK

The Metadata SDK includes the following predefined types that can be used to describe  ML workflows:

* [DataSet](https://kubeflow-metadata.readthedocs.io/en/latest/source/md.html#kubeflow.metadata.metadata.DataSet) to capture metadata for a dataset that forms the input into or the output of a component in your workflow.
* [Execution](https://kubeflow-metadata.readthedocs.io/en/latest/source/md.html#kubeflow.metadata.metadata.Execution) to capture metadata for an execution (run) of your ML workflow.
* [Metrics](https://kubeflow-metadata.readthedocs.io/en/latest/source/md.html#kubeflow.metadata.metadata.Metrics) to capture metadata for the metrics used to evaluate an ML model.
* [Model](https://kubeflow-metadata.readthedocs.io/en/latest/source/md.html#kubeflow.metadata.metadata.Model) to capture metadata for an ML model that your workflow produces.

## Metadata UI

* A list of logged artifacts and the details of each individual artifact is found in the **Artifact Store** on the Kubeflow UI.
* Navigate to [ml.cern.ch](https://ml.cern.ch) in a browser.
* Click **Artifact Store** in the left-hand navigation panel:
![metadata-ui-option](images/metadata-ui-option.png)
* The Artifacts screen opens and displays a list of items for all the metadata events that the workflows have logged.
The following examples show the items that appear after running the demo notebook described above:
![metadata-artifacts-list](images/metadata-artifacts-list.png)
* Example of **model** metadata with the name “MNIST”
![metadata-model](images/metadata-model.png)
* Example of **metrics** metadata with the name “MNIST-evaluation”
![metadata-metrics](images/metadata-metrics.png)
* Example of **dataset** metadata with the name “mytable-dump”
![metadata-dataset](images/metadata-dataset.png)

## Additional

An additional example of running Metadata SDK is shown in the chapter [Model Storage](https://ml.docs.cern.ch/storing).  
Demo notebook is provided [here](https://gitlab.cern.ch/ai-ml/examples/-/blob/master/model-storage/model_storage.ipynb).  
